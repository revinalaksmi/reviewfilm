<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id', 'film_id', 'message'];

    public function User(){
    	return $this->belongsTo(User::class);
    }
}
