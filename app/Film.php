<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $fillable = ['title', 'category_id','gambar', 'content'];

    public function Category(){
    	return $this->belongsTo(Category::class);
    }
    public function Comment(){
    	return $this->hasMany(Comment::class);
    }
}
