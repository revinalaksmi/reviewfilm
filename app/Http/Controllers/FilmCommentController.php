<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;
use App\Comment;

class FilmCommentController extends Controller
{
     public function store(Request $request, $id){
    	$film = Film::find($id);
    	$this->validate(request(),[
    		'message' => 'required'
    		]);
    	Comment::create([
    		'user_id' => auth()->id(),
    		//Auth::user()->id;
    		'film_id' => $film->id,
    		'message' => $request->message
    		]);
    	return redirect()->back();
}
}
?>
