<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;


use App\Category;
use App\Film;

class FilmController extends Controller
{
    public function __construct() {
		$this->middleware('auth');
	}

    public function index(){
    	$films = Film::all();

    	return view('film', compact('films'));
    }

    public function create(){
    	$categories = Category::all();
    	//dd($categories);
    	return view('createfilm', compact('categories'));
    }

    public function store(Request $request) {
    	$request->validate([
    		'title' => 'required',
            'gambar' => 'required',
    		'content' => 'required|min:6|string',
    		]);
    	//dd(request('content'));

        $uploadFile = $request->file('gambar');
        $path = $uploadFile->store('public/upload/fotoFilm');

    	Film::create([
    		'title' => $request->title,
    		'category_id' => request('category'),
            'gambar' => $path,
    		'content' => request('content'),
    		]);
    	return redirect()->route('film');
    }

    public function edit($id){
    	$film= Film::where('id',$id)->first();

    	$categories = Category::all();
    	return view('editfilm', compact('film', 'categories'));
    }

    public function detail($id){
        $film= Film::where('id',$id)->first();
       // $category = Category::where('id', $film->category_id)->first();
        return view('detailfilm', compact('film'));
    }

    public function update(Request $request, $id){
    	$film=Film::find($id)->first();
    	$request->validate([
    		'title' => 'required|string',
            'gambar' => 'required',
    		'content' => 'required|min:6|string',
    		]);
        if ($film->gambar == null) {
             $uploadFile = $request->file('gambar');
        $path = $uploadFile->store('public/upload/fotoFilm');
        }
        else{
            Storage::delete($film->gambar);
            $uploadFile = $request->file('gambar');
        $path = $uploadFile->store('public/upload/fotoFilm');
        }

    		$film->update([
    			'title' => $request->title,
    			'category_id' => request('category'),
                'gambar' => $path,
    			'content' => request('content'),
    		]);

    		return redirect()->route('film');
    }

    public function destroy($id){
    	$film = Film::where('id', $id)->first();
        //dd($film);
    	$film->delete();

    	return redirect()->back();
    }
}
