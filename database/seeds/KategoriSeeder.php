<?php

use Illuminate\Database\Seeder;
use App\Category;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
        	'nama' => 'Comedy',
        	'slug' => str_slug('comedy'),
        	]);

        Category::create([
        	'nama' => 'Action',
        	'slug' => str_slug('action'),
        	]);
    }
}
