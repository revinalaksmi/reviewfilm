<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/film','FilmController@index')->name('film');

Route::get('/film/create','FilmController@create')->name('film.create');

Route::post('/film/create','FilmController@store')->name('film.store');

Route::post('/film/{id}', 'FilmCommentController@store')->name('film.comment');

Route::get('/film/{id}/edit', 'FilmController@edit')->name('film.edit');

Route::patch('/film/{id}/update', 'FilmController@update')->name('film.update');

Route::delete('/film/{id}/delete', 'FilmController@destroy')->name('film.delete');

Route::get('/film/{id}/detail', 'FilmController@detail')->name('film.detail');
