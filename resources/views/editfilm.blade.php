@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('film.update', $film->id)}}" enctype="multipart/form-data" method="POST">
                        {{method_field('PATCH')}}
                        @csrf
                        <div class="form-group">
                            <label for="title">Judul</label>
                            <input class="form-control" value="{{$film->title}}" type="text" name="title" placeholder="Judul">
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select class="form-control" name="category" id="category">
                                @foreach($categories as $category)
                                <option value="{{$category->id}}"
                                    @if($film->category_id === $category->id)
                                        selected
                                    @endif
                                    >{{$category->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Gambar</label>
                             <img src="{{ Storage::url($film->gambar)}}" class="img-thumbnail">
                            <input class="form-control" type="file" name="gambar" placeholder="Gambar">

                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea class="form-control"  name="content" id="content" cols="30" rows="3" placeholder="content">{{$film->content}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
