@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($films as $u)
    <br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $u->title }} || {{ $u->id }}</div>
                <div class="card-body">
                    <center><img src="{{ Storage::url($u->gambar)}}" width="250" height="250">
                    </center>
                    <form method="post" action="{{ route('film.delete', $u->id)}}">
                         @csrf
                         @method('delete')
                        <button type="submit">Delete</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
